package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    
    
    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String run(String[] args) {
        String elementer = "";
        for (String string : args) {
            elementer += string + " ";
        }
        return elementer;
    }
    
}
